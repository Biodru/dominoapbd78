using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Domino.DTOs;
using Domino.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Domino.Services
{
    public class DbServices : IDbServices
    {
        private readonly MainDbContext _context;
        private readonly IConfiguration _configuration;
        
        public DbServices(IConfiguration configuration, MainDbContext context)
        {
            _configuration = configuration;
            _context = context;
        }
        
        //MARK: Pobieranie lekarzy
        public async Task<IEnumerable<Doctor>> GetAllDoctors()
        {
            var doctors = _context.Doctors;

            return doctors;
        }
        //MARK: Usuń lekarza
        public DeleteDoctorResponseDto DeleteDoctor(int id)
        {
            Doctor doctor = _context.Doctors.Single(d => d.IdDoctor == id);

            if (doctor == null)
            {
                return new DeleteDoctorResponseDto
                {
                    IsDeleted = false,
                    Message = "Nie ma leakrza o podanym id",
                };
            }

            _context.Doctors.Remove(doctor);
            _context.SaveChanges();
            return new DeleteDoctorResponseDto
            {
                IsDeleted = true,
                Message = $"Usunięto doktora: {doctor.FirstName} {doctor.LastName}",
            };
        }
        //MARK: Dodaj lekarza

        public AddNewDoctorDto AddNewDoctor(Doctor nDoctor)
        {
            if (_context.Doctors.Any(d => d.IdDoctor == nDoctor.IdDoctor))
            {
                return new AddNewDoctorDto
                {
                    Exists = true,
                    Message = "Doktor o podanym id już istnieje",
                };
            }

            _context.Doctors.Add(nDoctor);
            _context.SaveChanges();
            return new AddNewDoctorDto
            {
                Exists = false,
                Message = $"Dodano nowego doktora: {nDoctor.FirstName} {nDoctor.LastName}"
            };
        }
        //MARK: Zaktualizuj lekarza
        public UpdateDoctorResponseDto UpdateDoctor(Doctor uDoctor)
        {
            var doctor = _context.Doctors.Single(d => d.IdDoctor == uDoctor.IdDoctor);
            if (doctor == null)
            {
                return new UpdateDoctorResponseDto()
                {
                    Exists = false,
                    Message = "Doktor o podanym id nie istnieje",
                };
            }

            doctor = uDoctor;
            _context.SaveChanges();
            return new UpdateDoctorResponseDto
            {
                Exists = true,
                Message = $"Zaktualizowano dane doktora {uDoctor.FirstName} {uDoctor.LastName}"
            };

        }

        public Prescription SearchForPrescription(SearchForPrescriptionRequestDto req)
        {
            var presc = _context.Prescriptions.Include(p => p.Doctor.FirstName == req.DoctorName && p.Doctor.LastName == req.DoctorLastname).Include(p => p.Patient.FirstName == req.PatientName && p.Patient.LastName == req.PatientLastname).Include(p => p.PrescriptionMedicaments.Equals(req.Medicaments)).Single();

            return presc;
        }

        public async Task<Tuple<bool, string>> Register(RegisterRequestDto register)
        {
            var user = await _context.Users.SingleAsync(u => u.Login == register.Login);
            if (user != null)
            {
                return new(false, "Błąd");
            }

            var nUSer = new User();
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            var hashedPassword = new PasswordHasher<User>().HashPassword(nUSer, register.Password+salt);
            string token = "";
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                token = Convert.ToBase64String(randomNumber);
            }
            nUSer.Email = register.Email;
            nUSer.Login = register.Login;
            nUSer.Password = hashedPassword;
            nUSer.RefreshToken = token;
            nUSer.RefreshTokenExpDate = DateTime.Now.AddDays(2);
            nUSer.Salt = salt.ToString();

            _context.Users.Add(nUSer);
            await _context.SaveChangesAsync();
            return new(true, "Zarejstrowano");
        }

        public async Task<Tuple<bool, string>> Login(LoginRequestDto login)
        {
            var user = await  _context.Users.FirstOrDefaultAsync(u => u.Login == login.Login);
            if (user == null)
            {
                return new(false, "Nie ma użytkownika o podanym loginie");
            }

            var checkPassword =
                new PasswordHasher<User>().VerifyHashedPassword(user, user.Password, login.Password + user.Salt);
            if (checkPassword == PasswordVerificationResult.Failed)
            {
                return new(false, "Błędna hasło");
            }
            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["SecretKey"]));

            SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            
            Claim[] claims =  {
                new Claim(ClaimTypes.Name, user.Login),
                new Claim(ClaimTypes.Role, "user"),
            };

            JwtSecurityToken token = new JwtSecurityToken(
                issuer: "https://localhost:5001",
                audience: "https://localhost:5001",
                claims: claims,
                expires: DateTime.Now.AddMinutes(10),
                signingCredentials: creds
            );
            
            string tokenGen = "";
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                tokenGen = Convert.ToBase64String(randomNumber);
            }

            user.RefreshToken = tokenGen;
            user.RefreshTokenExpDate = DateTime.Now.AddDays(2);
            await _context.SaveChangesAsync();

            return new(true, "Zalogowano");
        }
    }
}