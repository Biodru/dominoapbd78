using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domino.DTOs;
using Domino.Models;

namespace Domino.Services
{
    public interface IDbServices
    {
        public Task<IEnumerable<Doctor>> GetAllDoctors();
        public DeleteDoctorResponseDto DeleteDoctor(int id);

        public AddNewDoctorDto AddNewDoctor(Doctor nDoctor);
        public UpdateDoctorResponseDto UpdateDoctor(Doctor uDoctor);
        public Prescription SearchForPrescription(SearchForPrescriptionRequestDto req);
        public Task<Tuple<bool, string>> Register(RegisterRequestDto register);
        public Task<Tuple<bool, string>> Login(LoginRequestDto login);
    }
}