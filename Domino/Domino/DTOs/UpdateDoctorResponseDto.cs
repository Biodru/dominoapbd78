namespace Domino.DTOs
{
    public class UpdateDoctorResponseDto
    {
        public bool Exists { get; set; }
        public string Message { get; set; }
    }
}