using System.Collections.Generic;
using Domino.Models;

namespace Domino.DTOs
{
    public class SearchForPrescriptionRequestDto
    {
        public string DoctorName { get; set; }
        public string DoctorLastname { get; set; }
        public string PatientName { get; set; }
        public string PatientLastname { get; set; }

        public ICollection<Medicament> Medicaments { get; set; }
    }
}