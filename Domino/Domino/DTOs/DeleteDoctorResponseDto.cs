namespace Domino.DTOs
{
    public class DeleteDoctorResponseDto
    {
        public bool IsDeleted { get; set; }
        public string Message { get; set; }
    }
}