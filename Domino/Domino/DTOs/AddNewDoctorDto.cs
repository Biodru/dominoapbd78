namespace Domino.DTOs
{
    public class AddNewDoctorDto
    {
        public bool Exists { get; set; }
        public string Message { get; set; }
    }
}