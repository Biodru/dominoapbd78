using Domino.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domino.EfConfigurations
{
    public class PrescriptionMedicamentEntityTypeConfiguration : IEntityTypeConfiguration<Prescription_Medicament>
    {
        public void Configure(EntityTypeBuilder<Prescription_Medicament> builder)
        {
            builder.HasKey(e => e.IdMedicament);
            builder.HasKey(e => e.IdPrescription);
            builder.Property(e => e.Details).IsRequired().HasMaxLength(100);

            builder.HasOne(e => e.Medicaments).WithMany(med => med.PrescriptionMedicaments)
                .HasForeignKey(e => e.IdMedicament);
            builder.HasOne(e => e.Prescriptions).WithMany(pre => pre.PrescriptionMedicaments)
                .HasForeignKey(e => e.IdPrescription);
        }
    }
}