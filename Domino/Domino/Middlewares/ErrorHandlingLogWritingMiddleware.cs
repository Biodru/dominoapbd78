using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Domino.Middlewares
{
    public class ErrorHandlingLogWritingMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlingLogWritingMiddleware(RequestDelegate next)
        {
            this._next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            } catch (Exception e)
            {
                context.Response.StatusCode = 500;
                await using StreamWriter sw = new StreamWriter("log.txt", true);
                sw.Write(e);
            }

            
        }
    }
}