using Microsoft.AspNetCore.Builder;

namespace Domino.Middlewares
{
    public static class ErrorhandlingLogWritingExtensions
    {
        public static IApplicationBuilder UseErrorHandlingLogWriter(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ErrorHandlingLogWritingMiddleware>();
        }
    }
}