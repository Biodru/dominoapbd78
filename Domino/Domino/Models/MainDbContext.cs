using System;
using System.Collections.Generic;
using Domino.EfConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Domino.Models
{
    public class MainDbContext : DbContext
    {
        public MainDbContext()
        {
            
        }

        public MainDbContext(DbContextOptions opt) : base(opt)
        {
            
        }

        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Medicament> Medicaments { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Prescription> Prescriptions { get; set; }
        public DbSet<Prescription_Medicament> PrescriptionMedicaments { get; set; }
        public DbSet<User> Users { get; set; }

        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        // {
        //     optionsBuilder.UseSqlServer("Data Source=db-mssql16.pjwstk.edu.pl;Initial Catalog=2019sbd;Integrated Security=True");
        // }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            List<Prescription> prescriptions = new List<Prescription>
            {
                new Prescription()
                {
                    IdPrescription = 1,
                    Date = DateTime.Now,
                    DueDate = DateTime.Now,
                    IdDoctor = 1,
                    IdPatient = 1,
                },
            };
            List<Doctor> doctors = new List<Doctor>
            {
                new Doctor
                {
                    IdDoctor = 1,
                    Email = "jk@gmail.com",
                    FirstName = "Jan",
                    LastName = "Kowalski",
                },
                new Doctor
                {
                    IdDoctor = 2,
                    Email = "anetaanetowska@gmail.com",
                    FirstName = "Aneta",
                    LastName = "Anetowska",
                },
            };
            List<Patient> patients = new List<Patient>
            {
                new Patient
                {
                    IdPatient = 1,
                    FirstName = "Adam",
                    LastName = "Abacki",
                    Birthdate = DateTime.Now,
                },
            };
            List<Prescription_Medicament> pm = new List<Prescription_Medicament>
            {
                new Prescription_Medicament
                {
                    IdMedicament = 1,
                    IdPrescription = 1,
                    Details = "Dwa razy dziennie",
                    Dose = null,
                },
            };
            List<Medicament> medicamentsList = new List<Medicament>
            {
                new Medicament
                {
                    IdMedicament = 1,
                    Description = "Opis leku",
                    Name = "LekKek",
                    Type = "Lek",
                },
            };
            modelBuilder.ApplyConfiguration(new DoctorEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PatientEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PrescriptionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PrescriptionMedicamentEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new MedicamentEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration());
            modelBuilder.Entity<Prescription>(e => e.HasData(prescriptions));
            modelBuilder.Entity<Doctor>(e => e.HasData(doctors));
            modelBuilder.Entity<Medicament>(e => e.HasData(medicamentsList));
            modelBuilder.Entity<Patient>(e => e.HasData(patients));
            modelBuilder.Entity<Prescription_Medicament>(e => e.HasData(pm));
        }
    }
}