using System;

namespace Domino.Models
{
    public class User
    {

        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string RefreshToken { get; set; }
        public DateTime? RefreshTokenExpDate { get; set; }
        
    }
}