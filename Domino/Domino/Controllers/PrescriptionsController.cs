using Domino.DTOs;
using Domino.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Domino.Controllers
{
    [ApiController]
    [Microsoft.AspNetCore.Components.Route("api/prescriptions")]
    public class PrescriptionsController : ControllerBase
    {

        private readonly IDbServices _db;

        public PrescriptionsController(IDbServices _db)
        {
            this._db = _db;
        }
        
        [AllowAnonymous]
        [HttpPut("registerPresc")]
        public IActionResult Register(RegisterRequestDto register)
        {
            var result = _db.Register(register);
            return result.Result.Item1 ? Ok(result.Result.Item2) : Problem(result.Result.Item2);
        }
        [AllowAnonymous] 
        [HttpPut("loginPresc")]
        public IActionResult Login(LoginRequestDto login)
        {
            var result = _db.Login(login);
            return result.Result.Item1 ? Ok(result.Result.Item2) : Problem(result.Result.Item2);
        }

        [Authorize]
        [HttpGet("show")]
        public IActionResult GetPrescription([FromBody]SearchForPrescriptionRequestDto req)
        {
            var result = _db.SearchForPrescription(req);
            return result != null ? Ok(result) : Problem("Nie znaleziono takiej recepty");
        }

    }
}