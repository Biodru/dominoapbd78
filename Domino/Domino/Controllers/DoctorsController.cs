using Domino.DTOs;
using Domino.Models;
using Domino.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Domino.Controllers
{
    [ApiController]
    [Microsoft.AspNetCore.Components.Route("api/doctors")]
    public class DoctorsController : ControllerBase
    {
        private readonly IDbServices _db;

        public DoctorsController(IDbServices db)
        {
            _db = db;
        }

        [AllowAnonymous]
        [HttpPut("register")]
        public IActionResult Register(RegisterRequestDto register)
        {
            var result = _db.Register(register);
            return result.Result.Item1 ? Ok(result.Result.Item2) : Problem(result.Result.Item2);
        }
        [AllowAnonymous] 
        [HttpPut("login")]
        public IActionResult Login(LoginRequestDto login)
        {
            var result = _db.Login(login);
            return result.Result.Item1 ? Ok(result.Result.Item2) : Problem(result.Result.Item2);
        }

        [Authorize]
        [HttpGet("showDoctors")]
        public IActionResult ShowDoctors()
        {
            return Ok(_db.GetAllDoctors());
        }

        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult DeleteDoctor([FromRoute] int id)
        {
            var result = _db.DeleteDoctor(id);
            return result.IsDeleted ? Ok(result.Message) : Problem(result.Message);
        }

        [Authorize]
        [HttpPost("addDoctor")]
        public IActionResult AddDoctor([FromBody] Doctor nDoctor)
        {
            var result = _db.AddNewDoctor(nDoctor);
            return result.Exists ? Problem(result.Message) : Ok(result.Message);
        }

        [Authorize]
        [HttpPut("updateDoctor")]
        public IActionResult UpdateDoctor([FromBody] Doctor uDoctor)
        {
            var result = _db.UpdateDoctor(uDoctor);
            return result.Exists ? Ok(result.Message) : Problem(result.Message);
        }
    }
}